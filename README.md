# y02l
[02環境リンクスの便利Webサイト](y02l.slimemoss.com)のリポジトリ群READMEです

## デプロイルール
### GitLab Runner
[dood](https://gitlab.com/y02l/gitlab-runner#dood)タグをつけてください

これにより、scriptがホストサーバ(slimemoss.com)のdocker.sockで実行されます

### docker-compose
プロジェクト名は```${CI_PROJECT_NAMESPACE}```を指定してください

```
docker-compose -p ${CI_PROJECT_NAMESPACE}
```

サービス名にはリポジトリ名を指定してください

すべてのサービスはdocker-composeによる```default```ネットワークに接続してください

これにより、サービス名(つまりリポジトリ名)を用いてサービス間通信をすることができます

#### 外部に出すコンテナ
想定する対象は[web](https://gitlab.com/y02l/web)です

ホストサーバ(slimemoss.com)に[nginx-proxy](https://github.com/nginx-proxy/nginx-proxy)と[letsencrypt-nginx-proxy-companion](https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion)が起動しています

サブドメインで外部に出すには、```nginx-proxy``` ネットワークに接続します

そして、環境変数```VIRTUAL_HOST```と```LETSENCRYPT_HOST```にドメイン名を設定します

## サービスのREADMEに書いて欲しい情報
* クイックスタート
* ローカルでのテスト
* APIリファレンス
* 依存するサービス

## サービスにあって欲しい機能
* 自動テスト
* 自動デプロイ

## APIリファレンスのテンプレート
### APIの説明
#### HTTPリクエスト
#### パスパラメータ
#### クエリパラメータ
#### リクエストボディ
#### レスポンス
